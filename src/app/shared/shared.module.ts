import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseLayoutComponent } from './components/base-layout/base-layout.component';
import { HeaderComponent } from './components/header/header.component';

@NgModule({
  declarations: [BaseLayoutComponent, HeaderComponent],
  exports: [BaseLayoutComponent],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
